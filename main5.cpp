#include <iostream>
#include <dlfcn.h>    // dlopen, dlerror, dlsym, dlclose


typedef void(*say_hello)(void);
const char* dllPath = "./libdlopen.so";

int main()
{
    for(int i=10;i<20;i++)
    {
        char dllPathsoft[100];
        snprintf(dllPathsoft,sizeof(dllPathsoft),"%s.%d.so",dllPath,i);

        void* handle = dlopen( dllPathsoft, RTLD_LAZY );
        if( !handle )
        {
            
            fprintf( stderr, "[%s](%d)@%s dlopen get error: %s\n", __FILE__, __LINE__, dllPathsoft,dlerror() );
            exit( EXIT_FAILURE );
        }
        else
        {
            std::cout << "handle addr is " << handle << std::endl;
        }
        say_hello fun = (say_hello)dlsym( handle, "say_hello" );
        fun();
        //std::cout << "handle addr is close " << handle << std::endl;
        // dlclose(handle);
    }
}