#include <iostream>
#include <dlfcn.h>    // dlopen, dlerror, dlsym, dlclose


typedef void(*say_hello)(void);
const char* dllPath = "./libdlopen.so.main7.so";

int main()
{
    for(int i=0;i<2;i++)
    {
        void* handle = dlopen( dllPath, RTLD_LAZY );
        if( !handle )
        {
            fprintf( stderr, "[%s](%d) dlopen get error: %s\n", __FILE__, __LINE__, dlerror() );
            exit( EXIT_FAILURE );
        }
        else
        {
            std::cout << "handle addr is " << handle << std::endl;
        }
        say_hello fun = (say_hello)dlsym( handle, "say_hello" );
        std::cout << "handle addr is close " << handle << std::endl;
        fun();
        
        std::cout << "wait to replace so " << handle << std::endl;
        getchar();
        // dlclose(handle);
    }
}