#include <iostream>
#include <dlfcn.h>    // dlopen, dlerror, dlsym, dlclose
typedef void(*say_hello)(void);
const char* dllPath = "./libdlopen.so";
int main()
{
    for(int i=0;i<10;i++)
    {
        void* handle = dlopen( dllPath, RTLD_LAZY );
        if( !handle )
        {
            fprintf( stderr, "[%s](%d) dlopen get error: %s\n", __FILE__, __LINE__, dlerror() );
            exit( EXIT_FAILURE );
        }
        else
        {
            std::cout << "handle addr is " << handle << std::endl;
        }
        say_hello fun = (say_hello)dlsym( handle, "say_hello" );
        
    }
}